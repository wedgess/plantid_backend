from werkzeug.security import generate_password_hash, check_password_hash

from shared import db

"""
This class is for the Users table. Uses SQLAlchemy to set up the database table and all columns along
with any foreign keys. Marshmallow is used to convert the class to JSON.
"""


class User(db.Model):
    __tablename__ = 'users'
    u_id = db.Column(db.String(50), primary_key=True)
    u_uname = db.Column(db.String(80))
    u_img_url = db.Column(db.String(120))
    u_pwd = db.Column(db.String(54))
    u_date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    comments = db.relationship('Comment', cascade="all, delete-orphan")
    threads = db.relationship('Thread', cascade="all, delete-orphan")
    history = db.relationship('History', cascade="all, delete-orphan")

    def __init__(self, id, username, password, img_url, date_created=db.func.current_timestamp()):
        self.u_id = id
        self.u_uname = username
        self.set_password(password)
        self.u_img_url = img_url
        self.u_date_created = db.func.current_timestamp()

    def set_password(self, password):
        self.u_pwd = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.u_pwd, password)

    def save(self):
        """Creates a given user."""
        from sqlalchemy.exc import IntegrityError
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except IntegrityError:
            return False

    def delete(self):
        """Deletes a given user."""
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_schema(many=False):
        # if returning a list of users many must be set as True
        return UserSchema(many=many)

    @staticmethod
    def get_user_by_username(uname):
        return User.query.filter_by(u_uname=uname).first()

    @staticmethod
    def validate_user(request):
        from app.routes import build_response
        from flask_api import status
        if 'u_id' not in request:
            return build_response(True, "Missing data u_id", ""), status.HTTP_400_BAD_REQUEST
        elif 'u_uname' not in request:
            return build_response(True, "Missing data u_uname", ""), status.HTTP_400_BAD_REQUEST
        elif 'u_pwd' not in request:
            return build_response(True, "Missing data u_pwd", ""), status.HTTP_400_BAD_REQUEST
        elif 'u_img_url' not in request:
            return build_response(True, "Missing data u_img_url", ""), status.HTTP_400_BAD_REQUEST


from marshmallow import Schema, fields


class UserSchema(Schema):
    # Fields to expose for Marshmallow to return JSON
    u_id = fields.Str()
    u_uname = fields.Str()
    u_img_url = fields.Str()
    u_pwd = fields.Str()
    u_date_created = fields.DateTime()
