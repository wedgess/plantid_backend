from marshmallow import Schema, fields

from shared import db

"""
This class is for the Threads table. Uses SQLAlchemy to set up the database table and all columns along
with any foreign keys. Marshmallow is used to convert the class to JSON.
"""


class Thread(db.Model):
    __tablename__ = 'threads'
    id = db.Column(db.String(50), primary_key=True)
    message = db.Column(db.String(800))
    author_id = db.Column(db.String(50), db.ForeignKey('users.u_id'))
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    t_title = db.Column(db.String(50))
    t_img_url = db.Column(db.String(80))
    t_solved = db.Column(db.Boolean)
    t_location = db.Column(db.String(40))
    comments = db.relationship('Comment', cascade="all, delete-orphan")

    def __init__(self, id, title, message, author_id, img_url, solved, location,
                 date_created=db.func.current_timestamp()):
        self.id = id
        self.t_title = title
        self.message = message
        self.author_id = author_id
        self.date_created = date_created
        self.t_solved = solved
        self.t_location = location
        self.t_img_url = img_url

    # save the thread to DB
    def save(self):
        from sqlalchemy.exc import IntegrityError
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except IntegrityError:
            return False

    def delete(self):
        """Deletes a given thread."""
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_schema(many=False):
        return ThreadSchema(many=many)

    @staticmethod
    def get_all():
        return Thread.query.all()

    @staticmethod
    def validate_thread(request):
        from app.routes import build_response
        from flask_api import status
        if 'id' not in request:
            return build_response(True, "Missing data id", ""), status.HTTP_400_BAD_REQUEST
        elif 'message' not in request:
            return build_response(True, "Missing data message", ""), status.HTTP_400_BAD_REQUEST
        elif 'author_id' not in request:
            return build_response(True, "Missing data author_id", ""), status.HTTP_400_BAD_REQUEST
        elif 't_img_url' not in request:
            return build_response(True, "Missing data t_img_url", ""), status.HTTP_400_BAD_REQUEST
        elif 't_solved' not in request:
            return build_response(True, "Missing data t_solved", ""), status.HTTP_400_BAD_REQUEST
        elif 't_location' not in request:
            return build_response(True, "Missing data t_location", ""), status.HTTP_400_BAD_REQUEST
        elif 't_title' not in request:
            return build_response(True, "Missing data t_title", ""), status.HTTP_400_BAD_REQUEST


class ThreadSchema(Schema):
    # Fields to expose for Marshmallow to return JSON
    id = fields.Str()
    message = fields.Str()
    author_id = fields.Str()
    date_created = fields.DateTime()
    t_img_url = fields.Str()
    t_title = fields.Str()
    t_solved = fields.Boolean()
    t_location = fields.Str()
