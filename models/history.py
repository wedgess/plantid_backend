from shared import db
"""
This class is for the History table. Uses SQLAlchemy to set up the database table and all columns along
with any foreign keys. Marshmallow is used to convert the class to JSON.
"""

class History(db.Model):
    __tablename__ = 'history'
    h_id = db.Column(db.String(50), primary_key=True)
    h_img_url = db.Column(db.String(120))
    h_date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    h_is_matched = db.Column(db.Boolean)
    h_location = db.Column(db.String(50))
    h_user_id = db.Column(db.String(50), db.ForeignKey('users.u_id'))
    h_plant_id = db.Column(db.String(50), db.ForeignKey('plants.p_id'))

    def __init__(self, img_url, user_id, is_matched=False, plant_id=-1, location=""):
        import uuid
        self.h_id = str(uuid.uuid4())
        self.h_img_url = img_url
        self.h_is_matched = is_matched
        self.h_user_id = user_id
        self.h_location = location
        self.h_plant_id = plant_id

    def save(self):
        """Creates a given history item."""
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Deletes a given history item."""
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_schema(many=False):
        return HistorySchema(many=many)

    @staticmethod
    def validate_history(request):
        from app.routes import build_response
        from flask_api import status
        if 'h_id' not in request:
            return build_response(True, "Missing data h_id", ""), status.HTTP_400_BAD_REQUEST
        elif 'h_img_url' not in request:
            return build_response(True, "Missing data h_img_url", ""), status.HTTP_400_BAD_REQUEST
        elif 'h_date_created' not in request:
            return build_response(True, "Missing data h_date_created", ""), status.HTTP_400_BAD_REQUEST
        elif 'h_is_matched' not in request:
            return build_response(True, "Missing data h_is_matched", ""), status.HTTP_400_BAD_REQUEST
        elif 'h_plant_id' not in request:
            return build_response(True, "Missing data h_plant_id", ""), status.HTTP_400_BAD_REQUEST

    @staticmethod
    def get_all_by_user(uid):
        return History.query.filter_by(h_user_id=uid).all()


from marshmallow import Schema, fields


class HistorySchema(Schema):
    # Fields to expose for Marshmallow to return JSON
    h_id = fields.Str()
    h_img_url = fields.Str()
    h_date_created = fields.DateTime()
    h_is_matched = fields.Boolean()
    h_user_id = fields.Str()
    h_plant_id = fields.Str()
    h_location = fields.Str()
