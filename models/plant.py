from marshmallow import Schema, fields

from shared import db

"""
This class is for the plants table. Uses SQLAlchemy to set up the database table and all columns along
with any foreign keys. Marshmallow is used to convert the class to JSON.
"""


class Plant(db.Model):
    __tablename__ = 'plants'
    p_id = db.Column(db.String(50), primary_key=True)
    p_img_url = db.Column(db.String(120))
    p_botanical_name = db.Column(db.String(60))
    p_common_name = db.Column(db.String(60))
    p_plant_type = db.Column(db.String(60))
    p_height = db.Column(db.String(10))
    p_width = db.Column(db.String(10))
    p_evergreen_or_deciduous = db.Column(db.String(30))
    p_flower_colors = db.Column(db.String(140))
    p_foliage_colors = db.Column(db.String(140))
    p_features = db.Column(db.String(140))
    p_description = db.Column(db.String(1000))
    p_sun_exposure = db.Column(db.String(100))
    p_shade_type = db.Column(db.String(100))
    p_hardiness = db.Column(db.String(30))
    p_maintenance = db.Column(db.String(200))
    p_soil_type = db.Column(db.String(150))
    p_attracts = db.Column(db.String(30))
    p_toxicity = db.Column(db.String(30))
    p_matches_plants = db.Column(db.String(130))
    p_effects = db.Column(db.String(130))
    p_wildlife_features = db.Column(db.String(130))
    p_collect_seed_time = db.Column(db.String(130))
    p_sow_time = db.Column(db.String(130))
    p_plant_time = db.Column(db.String(130))
    p_prune_time = db.Column(db.String(130))
    p_flower_time = db.Column(db.String(130))

    def __init__(self, **entries):
        self.__dict__.update(entries)
        import uuid
        self.p_id = str(uuid.uuid4())

    # saves the plant in DB
    def save(self):
        from sqlalchemy.exc import IntegrityError
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except IntegrityError:
            return False

    @staticmethod
    def get_schema(many=False):
        # many must be set Trie is getting a list of items
        return PlantSchema(many=many)

    @staticmethod
    def get_all():
        return Plant.query.all()


class PlantSchema(Schema):
    # Fields to expose for Marshmallow to parse as JSON
    p_id = fields.Str()
    p_img_url = fields.Str()
    p_common_name = fields.Str()
    p_botanical_name = fields.Str()
    p_plant_type = fields.Str()
    p_height = fields.Str()
    p_width = fields.Str()
    p_evergreen_or_deciduous = fields.Str()
    p_flower_colors = fields.Str()
    p_foliage_colors = fields.Str()
    p_features = fields.Str()
    p_description = fields.Str()
    p_sun_exposure = fields.Str()
    p_shade_type = fields.Str()
    p_hardiness = fields.Str()
    p_maintenance = fields.Str()
    p_soil_type = fields.Str()
    p_attracts = fields.Str()
    p_toxicity = fields.Str()
    p_matches_plants = fields.Str()
    p_effects = fields.Str()
    p_wildlife_features = fields.Str()
    p_collect_seed_time = fields.Str()
    p_sow_time = fields.Str()
    p_plant_time = fields.Str()
    p_prune_time = fields.Str()
    p_flower_time = fields.Str()
