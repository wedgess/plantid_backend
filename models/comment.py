from marshmallow import Schema, fields

from shared import db

"""
This class is for comments. Uses SQLAlchemy to set up the database table and all columns along
with any foreign keys. Marshmallow is used to convert the class to JSON.
"""

class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.String(50), primary_key=True)
    message = db.Column(db.String(800))
    author_id = db.Column(db.String(50), db.ForeignKey('users.u_id'))
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    c_thread_id = db.Column(db.String(50), db.ForeignKey('threads.id'))
    c_is_answer = db.Column(db.Boolean)

    def __init__(self, id, message, author_id, is_answer, thread_id, date_created=db.func.current_timestamp()):
        self.id = id
        self.message = message
        self.author_id = author_id
        self.date_created = date_created
        self.c_is_answer = is_answer
        self.c_thread_id = thread_id

    def save(self):
        from sqlalchemy.exc import IntegrityError
        try:
            db.session.add(self)
            db.session.commit()
            return True
        except IntegrityError:
            return False

    def delete(self):
        """Deletes a given comment."""
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_schema(many=False):
        # many must be set as True if getting a list
        return CommentSchema(many=many)

    @staticmethod
    def get_all_by_thread_id(tid):
        return Comment.query.filter_by(c_thread_id=tid).all()

    @staticmethod
    def validate_comment(request):
        from app.routes import build_response
        from flask_api import status
        if 'id' not in request:
            return build_response(True, "Missing data id", ""), status.HTTP_400_BAD_REQUEST
        elif 'message' not in request:
            return build_response(True, "Missing data message", ""), status.HTTP_400_BAD_REQUEST
        elif 'author_id' not in request:
            return build_response(True, "Missing data author_id", ""), status.HTTP_400_BAD_REQUEST
        elif 'date_created' not in request:
            return build_response(True, "Missing data date_created", ""), status.HTTP_400_BAD_REQUEST
        elif 'c_is_answer' not in request:
            return build_response(True, "Missing data c_is_answer", ""), status.HTTP_400_BAD_REQUEST
        elif 'c_thread_id' not in request:
            return build_response(True, "Missing data c_thread_id", ""), status.HTTP_400_BAD_REQUEST


class CommentSchema(Schema):
    # Fields to expose for Marshmallow as JSON
    id = fields.Str()
    message = fields.Str()
    author_id = fields.Str()
    date_created = fields.DateTime()
    c_thread_id = fields.Str()
    c_is_answer = fields.Boolean()
