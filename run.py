import argparse
import os

from flask import Flask

from app.routes import set_app_routes
from instance.config import app_config
from shared import db, ma, ROOT_DIR


"""
This is the main file to run to start the REST API.
"""


def create_app(config_type):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_type])
    app.config.from_pyfile(os.path.join(ROOT_DIR, 'instance', 'config.py'))
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    ma.init_app(app)
    set_app_routes(app)
    return app


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='PlantID backend REST API.')
    parser.add_argument('config_type',
                        help='The configuration type: development, staging, testing or production')
    args = parser.parse_args()

    create_app(args.config_type).run(host="192.168.1.23")
