import os
from shared import ROOT_DIR

"""
Handles app configuration.
There are 4 configuration types:

@DevelopmentConfig - should be used when developing the API
@TestingConfig - should be used when running test as the app will then use the test DB
@StagingConfig - should be before deploying to server and after development
@ProductionConfig - should be used when deploying to a server
"""

# root directory must be redefined here or it causes circular dependencies
# ROOT_DIR = os.path.join(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir))

class Config(object):
    # Parent configuration class.
    DEBUG = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(ROOT_DIR, 'databases', 'plantpal.sqlite'))
    UPLOADS_DEFAULT_DEST = '{}/app/static/images'.format(ROOT_DIR)
    UPLOADS_DEFAULT_URL = 'http://localhost:5000/static/images'


class DevelopmentConfig(Config):
    # Configurations for Development.
    DEBUG = True


class TestingConfig(Config):
    # Configurations for Testing, with a separate test database.
    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(
        os.path.join(ROOT_DIR, 'databases/plantpal-test.sqlite'))
    DEBUG = True


class StagingConfig(Config):
    # Configurations for Staging.
    DEBUG = True


class ProductionConfig(Config):
    # Configurations for Production.
    DEBUG = False
    TESTING = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
}
