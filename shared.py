import os

from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

"""
Shared dependencies for the project, only want one instance of SQLAlchemy and Marshmallow.
Also get a reference to project root to be used by other files and the credentials file.
"""

db = SQLAlchemy()
ma = Marshmallow()

# project root and credentials path
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CREDENTIALS_FILE = os.path.join(ROOT_DIR, 'credentials.txt')
