# About

This is a REST API for [PlantID frontend](https://bitbucket.org/wedgess/plantid_frontend). It is built using Flask, SQLAlchemy and Marshmallow. The database used is an SQLite database. The endpoints for the REST API are documented below.


----------------------------------

## Endpoints

| **Endpoint**                	| **Parameters**                     	| **Body**                                       	| **Action** 	| **Description**                                                     	|
|-----------------------------	|------------------------------------	|------------------------------------------------	|------------	|---------------------------------------------------------------------	|
| /users/:uid                 	| uid - users id                     	|                                                	| GET        	| Get a specific user                                                 	|
| /users/:uid                 	| uid - users id                     	| User object (JSON)                             	| PUT        	| Updates a user                                                      	|
| /users/:uid                 	| uid - users id                     	|                                                	| DELETE     	| Deletes a specific user                                             	|
| /users/register             	|                                    	| User object (JSON) and image file as form data 	| POST       	| Registers a user - inserts the user in DB and uploads profile image 	|
| /users/login                	|                                    	| username and password as JSON                  	| POST       	| Attempts to log user in                                             	|
| /users/:uid/threads         	| uid - users id                     	|                                                	| GET        	| Gets all threads for a specific user                                	|
| /users/:uid/history         	| uid - users id                     	|                                                	| GET        	| Gets a users history                                                	|
| /users/:uid/history         	| uid - users id                     	| History object (JSON)                          	| POST       	| Adds a user history item                                            	|
| /users/:uid/history         	| uid - users id                     	|                                                	| DELETE     	| Deletes a user hostory item                                         	|
| /users/:uid/history/:hid    	| uid - users id hid - history id    	| History object (JSON)                          	| PUT        	| Updates a users history item                                        	|
| /plants                     	| Requires Basic HTTP Authentication 	| Array of plant objects (JSON)                  	| POST       	| Creates plants                                                      	|
| /plants                     	|                                    	|                                                	| GET        	| Gets all plants                                                     	|
| /plants/:pid                	| pid - plant id                     	|                                                	| GET        	| Gets a specific plant                                               	|
| /threads                    	|                                    	|                                                	| GET        	| Gets all threads                                                    	|
| /threads                    	|                                    	| Thread object (JSON)                           	| POST       	| Creates a thread                                                    	|
| /threads/:tid               	| tid - thread id                    	|                                                	| GET        	| Gets a specific thread                                              	|
| /threads/:tid               	| tid - thread id                    	| Thread object (JSON)                           	| PUT        	| Updates a thread                                                    	|
| /threads/:tid               	| tid - thread id                    	|                                                	| DELETE     	| Deletes a thread                                                    	|
| /threads/:tid/comments      	| tid - thread id                    	|                                                	| GET        	| Gets all comments for a specific thread                             	|
| /threads/:tid/comments      	| tid - thread id                    	| Comment object (JSON)                          	| POST       	| Creates a comment                                                   	|
| /threads/:tid/comments/:cid 	| tid - thread id cid - comment id   	|                                                	| GET        	| Gets a specific comment by id                                       	|
| /threads/:tid/comments/:cid 	| tid - thread id cid - comment id   	| Comment object (JSON)                          	| PUT        	| Updates a specific comment by id                                    	|
| /threads/:tid/comments/:cid 	| tid - thread id cid - comment id   	|                                                	| DELETE     	| Deletes a comment by id                                             	|

-----------------------------------------------------

### Running REST API

Before running the project make sure all the requirements are installed. To do this run the following command:
```
pip3 install -U -r requirements.txt
```

You must also have the **credentials.txt** file in the projects root. This file takes the format:```myusername mypassword``` the space between the username and password is essential.

You must specify the application configuration to run which is passed as an argument when running the application. There are 4 different configuration options available:
- development
- staging
- production
- testing

For example to run the REST API using the development configuration execute the following command:

```
python3.6 run.py development
```


----------------------------------------------------

## Migration

When making changes to the database schema you will need to migrate the database to avoid destruction of the current data in the database.

To set up migration and create the migration directory the following command must be executed (only necessary if setting up migrations(no migration folder)):
```
python3.6 manage.py db init

```

The below command should be executed to migrate the database:
```
python3.6 manage.py db migrate
```

Then to upgrade the database:
```
python3.6 manage.py db upgrade
```

----------------------------------------------------

# Testing

This project contains a number of unit tests to test the endpoints.
The APP_SETTINGS variable is set in each of the tests by default so does not need to be exported in terminal. Make sure you are in the project root and then execute the following command:

```
python3.6 -m pytest tests/

```
