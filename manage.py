import os

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager  # class for handling a set of commands

from app import db, create_app

"""
This file handles database migrations for the app. And will auto-generate
the migrations folder on first run.

manage.py db init - This command sets up the migration directory and folder (only to be run once)
manage.py db migrate - This command is used to perform migration to new version
manage.py db upgrade - This command is used to uprage the database after the above command is run.
"""

app = create_app(config_name=os.getenv('APP_SETTINGS'))
migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
