import json
import unittest
from base64 import b64encode

from flask_api import status

from run import create_app
from shared import db


class PlantsTestCase(unittest.TestCase):
    """This class represents the plants test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_type="testing")
        self.client = self.app.test_client
        dicta = dict(p_id="7b25314e3fbb434ea55fd5cc6423fe53",
                     p_img_url="http://image1.gardenersworld.com:80/image.jpg?tag=30a86472-2d4e-49c4-bbc5-6e15a07a4967&width=768&quality=80",
                     p_botanical_name="Digitalis purpurea", p_common_name="Foxglove", p_plant_type="Perennial",
                     p_evergreen_or_deciduous="Deciduous", p_height="150cm", p_width="45cm", p_flower_colors="Purple",
                     p_foliage_colors="Green", p_features="Attractive to wildlife, Flowers",
                     p_description="Digitalis purpurea is a native European woodland plant with spikes of tubular purple flowers with a spotted throat.",
                     p_sun_exposure="Full sun, Dappled shade, Partial shade, Full shade", p_shade_type="Damp",
                     p_hardiness="Hardy", p_maintenance="Prefers rich soil", p_soil_type="Well drained/light/sandy",
                     p_attracts="Beneficial insects,Birds", p_toxicity="People,Cats,Dogs",
                     p_matches_plants="Geranium Max Frei,Asplenium scolopendrium",
                     p_effects="Effects: Harmful if ingestested",
                     p_wildlife_features="Wildlife features: Nectar/pollen rich", p_collect_seed_time="July,August",
                     p_sow_time="July,August", p_plant_time="March,April,September,October", p_prune_time="August",
                     p_flower_time="May,June,July")
        dictb = dict(p_id="a7bf41810e5e48d0b29fc8e040a2a337",
                     p_img_url="http://image1.gardenersworld.com:80/image.jpg?tag=8cb4dea4-90b3-4748-883b-5f686f3e5f71&width=768&quality=80",
                     p_botanical_name="Clematis 'Negritjanka'", p_common_name="", p_plant_type="Climber",
                     p_evergreen_or_deciduous="Deciduous", p_height="150cm", p_width="45cm", p_flower_colors="Purple",
                     p_foliage_colors="Green", p_features="Attractive to wildlife, Flowers",
                     p_description="Digitalis purpurea is a native European woodland plant with spikes of tubular purple flowers with a spotted throat.",
                     p_sun_exposure="Full sun, Dappled shade, Partial shade, Full shade", p_shade_type="Damp",
                     p_hardiness="Hardy", p_maintenance="Prefers rich soil", p_soil_type="Well drained/light/sandy",
                     p_attracts="Beneficial insects,Birds", p_toxicity="People,Cats,Dogs",
                     p_matches_plants="Geranium Max Frei,Asplenium scolopendrium",
                     p_effects="Effects: Harmful if ingestested",
                     p_wildlife_features="Wildlife features: Nectar/pollen rich", p_collect_seed_time="July,August",
                     p_sow_time="July,August", p_plant_time="March,April,September,October", p_prune_time="August",
                     p_flower_time="May,June,July")
        self.plants = dict(data=[dicta, dictb])
        self.identification = dict(i_id="jsahdjhfsadfasd", i_img_url="", i_date_created="", i_is_match="",
                                   i_user_id="sadfdgasdfsdad", i_plant_id="")
        # binds the app to the current context
        with self.app.app_context():
            db.session.close()
            db.drop_all()
            db.create_all()

    def test_plants_creation_success(self):
        """Test API - can create a plant (POST request)"""
        self.assertEqual(status.HTTP_201_CREATED, self.create_plants_request().status_code)

        # test empty plant data -- ERROR
        res_no_data = self.create_plants_request(empty=True)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res_no_data.status_code)

    def test_get_all_plants(self):
        """Test API - can get all plants (POST request)"""
        # get plants when empty -- ERROR
        res_no_content = self.client().get('/plants')
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_no_content.status_code)

        self.assertEqual(status.HTTP_201_CREATED, self.create_plants_request().status_code)

        # get all plants -- OK
        res_no_content = self.client().get('/plants')
        self.assertEqual(status.HTTP_200_OK, res_no_content.status_code)

    def test_get_plant_by_id(self):
        res_plants = self.create_plants_request()
        self.assertEqual(status.HTTP_201_CREATED, res_plants.status_code)

        d = json.loads(self.client().get('/plants').data)
        pid = list(d.values())[0][0]['p_id']

        res = self.client().get('/plants/{}'.format(pid))
        self.assertEqual(status.HTTP_200_OK, res.status_code)


    def create_plants_request(self, empty=False):
        # create plants -- OK
        from app.routes import read_credentials
        credentials = read_credentials()
        auth = ''
        for username, password in credentials.items():
            auth = '{}:{}'.format(username, password)
        return self.client().post('/plants', data='' if empty else json.dumps(self.plants), content_type='application/json',
                                  headers={"Authorization": "Basic {user}".format(
                                      user=b64encode(auth.encode('ascii')).decode('utf-8'))})

    def test_upload_identification(self):
        pass

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
