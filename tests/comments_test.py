import json
import unittest

from flask_api import status

from run import create_app
from shared import db


class CommentsTestCase(unittest.TestCase):
    """This class represents the comments test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_type="testing")
        self.client = self.app.test_client
        c1 = dict(id="ospdfsdmfklmsfjsdfsjkjkjsdj", c_thread_id="skjfjsfsjdaafakdfmkmdsdfmskdm",
                  author_id="sadfdgasdfsdad", date_created="01/12/2017",
                  c_is_answer=True,
                  message="Some really helpfull message which was the answer to the threads question.")
        c2 = dict(id="ksjfkasjdfnasdjfksklsafsd", c_thread_id="skjfjsfsjdaafakdfmkmdsdfmskdm",
                  author_id="sadfdgasdfsdad", date_created="01/12/2017",
                  c_is_answer=False, message="Some really long message. Not the answer")
        c3 = dict(id="dhfahsfsadhfjshdjfkjshdjk", c_thread_id="skjfjsfsjdaafakdfmkmdsdfmskdm",
                  author_id="sadfdgasdfsdad", date_created="01/12/2017",
                  c_is_answer=False, message="Some really long message. Ao useless answer")
        self.comment = c1
        self.comments = [c1, c2, c3]

        # binds the app to the current context
        with self.app.app_context():
            db.session.close()
            db.drop_all()
            db.create_all()

    def test_comment_creation_success(self):
        """Test API - can create a comment (POST request)"""

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # create thread -- OK
        thread = dict(id="skjfjsfsjdaafakdfmkmdsdfmskdm", author_id="sadfdgasdfsdad",
                      t_img_url="http://image1.gardenersworld.com",
                      date_created="01/12/2017", t_solved=False, t_location="Dublin", t_title="Help identify!",
                      message="Some message")
        res = self.client().post('/threads', data=json.dumps(thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)

        # create comment - incorrect comment thread id compared to tid param -- ERROR
        self.comment['c_thread_id'] = "asdhfjhsashj"
        res_comment = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(self.comment),
                                         content_type='application/json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res_comment.status_code)

        # create comment -- OK
        self.comment['c_thread_id'] = "skjfjsfsjdaafakdfmkmdsdfmskdm"
        res_comment = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(self.comment),
                                         content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_comment.status_code)

        # response - contains comment object  -- OK
        response_data = json.loads(res_comment.get_data(as_text=True))['data']
        # remove dates created as they are created on creation
        response_data.pop('date_created', None)
        self.comment.pop('date_created', None)
        self.assertTrue(json.dumps(self.comment, sort_keys=True) == json.dumps(response_data, sort_keys=True))

        # comment exists -- ERROR
        self.comment['date_created'] = "12/12/2017"
        res_exists = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(self.comment),
                                        content_type='application/json')
        # comment will be updated if duplicate to fix for syncing client side
        self.assertEqual(status.HTTP_200_OK, res_exists.status_code)
        # self.assertEqual(status.HTTP_409_CONFLICT, res_exists.status_code)

        # comment validation -- ERROR
        self.comment.pop('message', None)
        res_fail = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(self.comment),
                                      content_type='application/json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res_fail.status_code)

    def test_get_threads_comments(self):
        ''' Test getting all comments by a thread id'''

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # create thread -- OK
        thread = dict(id="skjfjsfsjdaafakdfmkmdsdfmskdm", author_id="sadfdgasdfsdad",
                      t_img_url="http://image1.gardenersworld.com",
                      date_created="01/12/2017", t_solved=False, t_location="Dublin", t_title="Help identify!",
                      message="Some message")
        res = self.client().post('/threads', data=json.dumps(thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)

        # add all thread comments -- OK
        for comment in self.comments:
            res_comment = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(comment),
                                             content_type='application/json')
            self.assertEqual(status.HTTP_201_CREATED, res_comment.status_code)

        # get all thread comments -- OK
        result = self.client().get('/threads/{}/comments'.format(thread['id']))
        self.assertEqual(status.HTTP_200_OK, result.status_code)

        # incorrect thread if : get all thread comments -- ERRO
        result = self.client().get('/threads/{}/comments'.format("dsasfsadf"))
        self.assertEqual(status.HTTP_204_NO_CONTENT, result.status_code)

    def create_user_request(self):
        user = dict(u_id="sadfdgasdfsdad", u_uname="gareth", u_pwd="password", u_img_url="")
        from io import BytesIO
        from os import path
        data = dict()
        data['json'] = json.dumps(user)
        with open(path.join(path.abspath(path.dirname(__file__)), 'images', '01.jpg'), 'rb') as fp:
            imgBytesIO = BytesIO(fp.read())
        data['u_img_url'] = (imgBytesIO, 'test.jpg')
        return self.client().post('/users/register', data=data, content_type='multipart/form-data')

    def test_can_update_comment(self):
        ''' Test getting all comments by a thread id'''

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # create thread -- OK
        thread = dict(id="skjfjsfsjdaafakdfmkmdsdfmskdm", author_id="sadfdgasdfsdad",
                      t_img_url="http://image1.gardenersworld.com",
                      date_created="01/12/2017", t_solved=False, t_location="Dublin", t_title="Help identify!",
                      message="Some message")
        res = self.client().post('/threads', data=json.dumps(thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)

        # add comment -- OK
        res_comment = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(self.comment),
                                         content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_comment.status_code)

        # update comment -- OK
        self.comment['message'] = "New message"
        res_update = self.client().put('/threads/{0}/comments/{1}'.format(thread['id'], self.comment['id']),
                                       data=json.dumps(self.comment),
                                       content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, res_update.status_code)

        # test correct data returned -- OK
        response_data = json.loads(res_update.get_data(as_text=True))['data']
        response_data.pop('date_created', None)
        self.comment.pop('date_created', None)
        self.assertTrue(json.dumps(self.comment, sort_keys=True) == json.dumps(response_data, sort_keys=True))

    def test_comment_deletion(self):
        """Test API - can delete a comment (DELETE request)"""

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # create thread -- OK
        thread = dict(id="skjfjsfsjdaafakdfmkmdsdfmskdm", author_id="sadfdgasdfsdad",
                      t_img_url="http://image1.gardenersworld.com",
                      date_created="01/12/2017", t_solved=False, t_location="Dublin", t_title="Help identify!",
                      message="Some message")
        res = self.client().post('/threads', data=json.dumps(thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)

        # create comment -- OK
        res_comment = self.client().post('/threads/{}/comments'.format(thread['id']), data=json.dumps(self.comment),
                                         content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_comment.status_code)

        # delete thread incorrect cid -- ERROR
        res = self.client().delete('/threads/{0}/comments/{1}'.format(thread['id'], "dsfasdfas"))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res.status_code)

        # delete thread -- OK
        res = self.client().delete('/threads/{0}/comments/{1}'.format(thread['id'], self.comment['id']))
        self.assertEqual(status.HTTP_200_OK, res.status_code)


def tearDown(self):
    """teardown all initialized variables."""
    with self.app.app_context():
        # drop all tables
        db.session.remove()
        db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
