import json
import os
import unittest

from flask_api import status

from run import create_app
from shared import db


class UsersTestCase(unittest.TestCase):
    """This class represents the users test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_type="testing")
        self.client = self.app.test_client
        self.user = dict(u_id="sadfdgasdfsdad", u_uname="gar", u_pwd="password", u_img_url="",
                         u_date_created="12/12/2017")

        # binds the app to the current context
        with self.app.app_context():
            db.session.close()
            db.drop_all()
            db.create_all()

    def create_user_request(self, has_img=True):
        from io import BytesIO
        data = dict()
        data['json'] = json.dumps(self.user)
        print("Has image is {}".format(has_img))
        if has_img:
            with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'images', '01.jpg'), 'rb') as fp:
                imgBytesIO = BytesIO(fp.read())
            data['u_img_url'] = (imgBytesIO, 'test.jpg')
        return self.client().post('/users/register', data=data, content_type='multipart/form-data')

    def update_user_request(self, has_img=True):
        from io import BytesIO
        data = dict()
        data['json'] = json.dumps(self.user)
        print("Has image is {}".format(has_img))
        if has_img:
            with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'images', '01.jpg'), 'rb') as fp:
                imgBytesIO = BytesIO(fp.read())
            data['u_img_url'] = (imgBytesIO, 'test.jpg')
        return self.client().put('/users/{}'.format(self.user['u_id']), data=data, content_type='multipart/form-data')

    def remove_uploaded_img(self, res):
        response_data = json.loads(res.get_data(as_text=True))['data']
        if "u_img_url" in response_data:
            filename = response_data['u_img_url'].split("/")[-1]
            path = os.path.join(os.path.abspath(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))),
                                'app', 'static', 'images', 'profile', filename)
            if os.path.exists(path):
                os.remove(path)

    def test_user_deletion(self):
        """Test API can delete an existing user. (DELETE request)."""

        # # deleting non existing user -- ERROR
        res_not_found = self.client().delete('/users/{}'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)
        self.remove_uploaded_img(res_create)

        # delete user -- OK
        res = self.client().delete('/users/{}'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_200_OK, res.status_code)

        # test if deleted -- ERROR
        res_not_found = self.client().get('/users/{}'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

    def test_user_creation_success(self):
        """Test API - can create a user (POST request)"""
        # create user - OK
        res = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)
        self.remove_uploaded_img(res)

        # response - contains user object OK
        response_data = json.loads(res.get_data(as_text=True))['data']
        response_data.pop('u_date_created', None)
        self.user.pop('u_date_created', None)  # remove date created
        response_data['u_pwd'] = "password"  # need to remove password a it gets hashed and wont be same
        print(json.dumps(response_data, sort_keys=True))
        # ensure new user object has image url set
        self.assertNotEqual(response_data['u_img_url'], "")
        # set the uploaded image to the user object
        self.user['u_img_url'] = response_data['u_img_url']
        self.assertTrue(json.dumps(self.user, sort_keys=True) == json.dumps(response_data, sort_keys=True))

        # username exists -- ERROR
        self.user['u_date_created'] = "12/12/2017"
        res_exists = self.create_user_request(False)
        self.assertEqual(status.HTTP_409_CONFLICT, res_exists.status_code)
        self.remove_uploaded_img(res_exists)

        # test validation - ERROR
        self.user.pop('u_uname', None)
        res_fail = self.create_user_request(False)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res_fail.status_code)
        self.remove_uploaded_img(res_fail)

    def test_user_login(self):
        res = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)
        self.remove_uploaded_img(res)

        login_dict = dict(username="gar", password="password")
        res_login_success = self.client().post('/users/login', data=json.dumps(login_dict),
                                               content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, res_login_success.status_code)

        # change to an invalid password
        login_dict['password'] = "test"
        res_login_fail = self.client().post('/users/login', data=json.dumps(login_dict),
                                            content_type='application/json')
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, res_login_fail.status_code)

    def test_can_get_user_by_id(self):
        """Test API can get a user by id (GET request)."""

        # non existing user with id - ERROR
        res_not_found = self.client().get('/users/{}'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

        # create the user - OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)
        self.remove_uploaded_img(res_create)

        # getting user OK
        res = self.client().get('/users/{}'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_200_OK, res.status_code)

    def test_can_get_users_threads(self):
        """Test API can get threads by a user id (GET request)."""

        # non existing threads by author id -- ERROR
        res_found = self.client().get('/users/{}/threads'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_found.status_code)

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)
        self.remove_uploaded_img(res_create)

        # create thread -- OK
        thread = dict(id="skjfjsfsjdaafakdfmkmdsdfmskdm", author_id="sadfdgasdfsdad",
                      t_img_url="http://image1.gardenersworld.com",
                      date_created="01/12/2017", t_solved=False, t_location="Dublin", t_title="Help identify!",
                      message="Some message")
        res = self.client().post('/threads', data=json.dumps(thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)

        #  get users threads -- OK
        res_found = self.client().get('/users/{}/threads'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_200_OK, res_found.status_code)

    def test_user_can_be_updated(self):
        """Test API can edit an existing bucketlist. (PUT request)"""

        # not existing user with id -- ERROR
        res_not_found = self.client().get('/users/{}'.format(self.user['u_id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

        # create user -- OK
        res_create = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)
        self.remove_uploaded_img(res_create)

        # update user -- OK
        self.user['u_uname'] = "John Doe"
        res_update = self.update_user_request()
        self.assertEqual(status.HTTP_200_OK, res_update.status_code)

        # check if username was updated
        self.assertEqual(self.user['u_uname'], json.loads(res_update.data)['data']['u_uname'])

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
