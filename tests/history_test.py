import json
import os
import unittest
import uuid

from flask_api import status

from run import create_app
from shared import db


class PlantsTestCase(unittest.TestCase):
    """This class represents the plants test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_type="testing")
        self.client = self.app.test_client
        self.user = dict(u_id="sadfdgasdfsdad", u_uname="gareth", u_pwd="password", u_img_url="",
                         u_date_created="12/12/2017")

        dicta = dict(h_id=str(uuid.uuid4()),
                     h_img_url="http://image1.gardenersworld.com:80/image.jpg?tag=30a86472-2d4e-49c4-bbc5-6e15a07a4967&width=768&quality=80",
                     h_is_matched=False,
                     h_user_id="sadfdgasdfsdad",
                     h_location="Dublin",
                     h_plant_id="-1")
        dictb = dict(h_id=str(uuid.uuid4()),
                     h_img_url="http://image1.gardenersworld.com:80/image.jpg?tag=30a86472-2d4e-49c4-bbc5-6e15a07a4967&width=768&quality=80",
                     h_is_matched=False,
                     h_user_id="sadfdgasdfsdad",
                     h_location="Dublin",
                     h_plant_id="-1")
        self.history = dict(data=[dicta, dictb])
        # binds the app to the current context
        with self.app.app_context():
            db.session.close()
            db.drop_all()
            db.create_all()

    def test_history_creation_success(self):
        user_creation_res = self.create_user_request()
        history_req = self.create_history_item_request(0)
        """Test API - can create a history (POST request)"""
        self.assertEqual(status.HTTP_201_CREATED, history_req.status_code)
        self.remove_profile_uploaded_img(user_creation_res)
        self.remove_history_uploaded_img(history_req)

    def test_get_users_history(self):
        user_creation_res = self.create_user_request()
        history_req_one = self.create_history_item_request(0)
        history_req_two = self.create_history_item_request(1)
        self.assertEqual(status.HTTP_201_CREATED, user_creation_res.status_code)
        self.assertEqual(status.HTTP_201_CREATED, history_req_one.status_code)
        self.assertEqual(status.HTTP_201_CREATED, history_req_two.status_code)
        self.assertEqual(status.HTTP_200_OK,
                         self.client().get('/users/{}/history'.format(self.user['u_id'])).status_code)
        self.remove_profile_uploaded_img(user_creation_res)
        self.remove_history_uploaded_img(history_req_one)
        self.remove_history_uploaded_img(history_req_two)

    def test_update_history_item(self):
        user_creation_res = self.create_user_request()
        self.assertEqual(status.HTTP_201_CREATED, user_creation_res.status_code)
        history_req_one = self.create_history_item_request(0)
        self.assertEqual(status.HTTP_201_CREATED, history_req_one.status_code)

        hist = json.loads(history_req_one.get_data(as_text=True))['data']
        # update history -- OK
        hist['h_is_matched'] = True
        res_update = self.client().put('/users/{0}/history/{1}'.format(self.user['u_id'], hist['h_id']), data=json.dumps(hist),
                                       content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, res_update.status_code)

        # check if username was updated
        self.assertEqual(hist['h_is_matched'], json.loads(res_update.data)['data']['h_is_matched'])

        self.remove_profile_uploaded_img(user_creation_res)
        self.remove_history_uploaded_img(history_req_one)

    def test_deleting_history(self):
        user_creation_res = self.create_user_request()
        history_req_one = self.create_history_item_request(0)
        history_req_two = self.create_history_item_request(1)

        self.assertEqual(status.HTTP_201_CREATED, user_creation_res.status_code)
        self.assertEqual(status.HTTP_201_CREATED, history_req_one.status_code)
        self.assertEqual(status.HTTP_201_CREATED, history_req_two.status_code)

        hist = json.loads(history_req_one.get_data(as_text=True))['data']
        hist2 = json.loads(history_req_two.get_data(as_text=True))['data']

        data_delete = [hist, hist2]
        res_delete = self.client().delete('/users/{0}/history'.format(self.user['u_id']),
                                       data=json.dumps(data_delete),
                                       content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, res_delete.status_code)

        self.remove_profile_uploaded_img(user_creation_res)
        self.remove_history_uploaded_img(history_req_one)
        self.remove_history_uploaded_img(history_req_two)

    def remove_profile_uploaded_img(self, res):
        response_data = json.loads(res.get_data(as_text=True))['data']
        if "u_img_url" in response_data:
            filename = response_data['u_img_url'].split("/")[-1]
            path = os.path.join(os.path.abspath(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))),
                                'app', 'static', 'images', 'profile', filename)
            if os.path.exists(path):
                os.remove(path)

    def remove_history_uploaded_img(self, res):
        response_data = json.loads(res.get_data(as_text=True))['data']
        if "h_img_url" in response_data:
            filename = response_data['h_img_url'].split("/")[-1]
            path = os.path.join(os.path.abspath(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))),
                                'app', 'static', 'images', 'plantids', filename)
            print("Removing image:{}".format(path))
            if os.path.exists(path):
                os.remove(path)

    def create_history_item_request(self, pos):
        from io import BytesIO
        data = dict()
        data['json'] = json.dumps(self.history.get(pos))
        with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'images', '01.jpg'), 'rb') as fp:
            imgBytesIO = BytesIO(fp.read())
        data['img_upload'] = (imgBytesIO, 'test.jpg')
        return self.client().post('/users/{}/history'.format(self.user['u_id']), data=data,
                                  content_type='multipart/form-data')

        # create single history item -- OK

    def create_user_request(self, has_img=True):
        from io import BytesIO
        data = dict()
        data['json'] = json.dumps(self.user)
        print("Has image is {}".format(has_img))
        if has_img:
            with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'images', '01.jpg'), 'rb') as fp:
                imgBytesIO = BytesIO(fp.read())
            data['u_img_url'] = (imgBytesIO, 'test.jpg')
        return self.client().post('/users/register', data=data, content_type='multipart/form-data')

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
