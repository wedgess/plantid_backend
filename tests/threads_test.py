import json
import unittest

from flask_api import status

from run import create_app
from shared import db


class ThreadsTestCase(unittest.TestCase):
    """This class represents the threads test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app(config_type="testing")
        self.client = self.app.test_client
        self.thread = dict(id="skjfjsfsjdaafakdfmkmdsdfmskdm", author_id="sadfdgasdfsdad",
                           t_img_url="http://image1.gardenersworld.com:80/image.jpg?tag=78978ff6-16aa-4e66-8008-86e92546ea83&width=768&quality=80",
                           date_created="01/12/2017", t_solved=False, t_location="Dublin", t_title="Help identify!",
                           message="Some really long message asking for the communities help in identifying the plant in the image. Never seen the plant before so would be great if someone could identify it for me!")

        # binds the app to the current context
        with self.app.app_context():
            db.session.close()
            db.drop_all()
            db.create_all()

    def test_thread_creation_success(self):
        """Test API - can create a thread (POST request)"""

        # create thread -- OK
        res = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res.status_code)

        # response - contains thread object  -- OK
        response_data = json.loads(res.get_data(as_text=True))['data']
        # remove dates so test passes - dates are updated on creation
        response_data.pop('date_created', None)
        self.thread.pop('date_created', None)
        self.assertTrue(json.dumps(self.thread, sort_keys=True) == json.dumps(response_data, sort_keys=True))

        # thread validation -- ERROR
        self.thread.pop('t_title', None)
        res_fail = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res_fail.status_code)

    def test_can_get_thread_by_id(self):
        """Test API can get a thread by id (GET request)."""

        # test non existing user with id -- ERROR
        res_not_found = self.client().get('/threads/{}'.format(self.thread['id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

        # create thread -- OK
        res_create = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # test thread retrieval -- OK
        res_not_found = self.client().get('/threads/{}'.format(self.thread['id']))
        self.assertEqual(status.HTTP_200_OK, res_not_found.status_code)

        # test correct data returned -- OK
        response_data = json.loads(res_create.get_data(as_text=True))['data']
        # remove dates so test passes - dates are updated on creation
        response_data.pop('date_created', None)
        self.thread.pop('date_created', None)
        self.assertTrue(json.dumps(self.thread, sort_keys=True) == json.dumps(response_data, sort_keys=True))

    def test_thread_can_be_updated(self):
        """Test API can edit an existing thread. (PUT request)"""

        # test not existing thread with id -- ERROR
        res_not_found = self.client().get('/threads/{}'.format(self.thread['id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

        # create thread -- OK
        res_create = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # test thread validation error -- ERROR
        self.thread.pop('t_title', None)
        res_fail = self.client().put('/threads/{}'.format(self.thread['id']), data=json.dumps(self.thread),
                                     content_type='application/json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, res_fail.status_code)

        # update thread -- OK
        self.thread['t_title'] = "Please Help Identify!!"
        res_update = self.client().put('/threads/{}'.format(self.thread['id']), data=json.dumps(self.thread),
                                       content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, res_update.status_code)

        # test response contains thread -- OK
        response_data = json.loads(res_update.get_data(as_text=True))['data']
        # remove dates so test passes - dates are updated on creation
        response_data.pop('date_created', None)
        self.thread.pop('date_created', None)
        self.assertTrue(json.dumps(self.thread, sort_keys=True) == json.dumps(response_data, sort_keys=True))

    def test_can_delete_thread_by_id(self):
        """Test API can delete a thread by id (DELETE request)."""

        # test non existing user with id --ERROR
        res_not_found = self.client().delete('/threads/{}'.format(self.thread['id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

        # create thread -- OK
        res_create = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # delete thread -- OK
        res = self.client().delete('/threads/{}'.format(self.thread['id']))
        self.assertEqual(status.HTTP_200_OK, res.status_code)

        # test if thread is retrieved successfully -- ERROR
        res_not_found = self.client().get('/threads/{}'.format(self.thread['id']))
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_found.status_code)

    def test_can_get_all_threads(self):
        # test not existing thread with id -- OK
        res_not_content = self.client().get('/threads')
        self.assertEqual(status.HTTP_204_NO_CONTENT, res_not_content.status_code)

        # create thread -- OK
        res_create = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_create.status_code)

        # create thread -- OK
        self.thread['id'] = "shadfas6786sakjashnlk"
        self.thread['date_created'] = "12/12/2017"
        res_create_two = self.client().post('/threads', data=json.dumps(self.thread), content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, res_create_two.status_code)

        # test existing threads -- OK
        res = self.client().get('/threads')
        self.assertEqual(status.HTTP_200_OK, res.status_code)

    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()
