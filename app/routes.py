import datetime
import json
import uuid

from flask import request, jsonify
from flask_api import status
from flask_httpauth import HTTPBasicAuth
from flask_uploads import UploadSet, IMAGES, configure_uploads

from models.comment import Comment
from models.history import History
from models.plant import Plant
from models.thread import Thread
from models.user import User

"""
This is the main routing file for the Flask REST API.
All endpoints are contained in this file.
"""


def set_app_routes(app):
    # Configure the image uploading via Flask-Uploads - one for history/ids and the other for profile images
    id_images = UploadSet('plantids', IMAGES)
    profile_images = UploadSet('profile', IMAGES)
    configure_uploads(app, (id_images, profile_images))
    # setup HTTP Authentication
    auth = HTTPBasicAuth()

    # TODO: Server name testing locally should use 192.168.1.23, server uses domain
    server_name = "garethwilliams.host"

    # make sure credentials.txt file is present or exit with error
    import os
    import sys
    from shared import CREDENTIALS_FILE
    if not os.path.exists(CREDENTIALS_FILE):
        sys.exit(
            '[ERROR] - credentials.txt file is missing - this file must be placed in the project root and takes the format: <username> <password>')

    # server_name = "garethwilliams.host"

    # used for getting the credentials to upload new plants to server
    @auth.get_password
    def get_pw(username):
        credentials = read_credentials()
        if username in credentials:
            return credentials.get(username)
        return None

    ##############################  USERS #######################################

    # endpoint to create new user
    @app.route("/users/register", methods=["POST"])
    def add_user():
        # data is sent in a form as an image is uploaded with user info
        request_data = json.loads(request.form.get("json"))
        # validate user object, if errors then return the issue in request body
        validation_errors = User.validate_user(request_data)
        if validation_errors:
            return validation_errors
        if not request_data['u_id']:
            request_data['u_id'] = str(uuid.uuid4())
        # if username already exists return an error 409 - Conflict
        username = request_data['u_uname']
        username_exists = User.query.filter_by(u_uname=username).first()
        if username_exists:
            return build_response(True, "Username {} already exists".format(username)), status.HTTP_409_CONFLICT
        if 'u_img_url' in request.files:
            # get the image and save it to profile directory
            file = request.files['u_img_url']
            file.filename = "{}.jpg".format(request_data['u_id'])
            filename = profile_images.save(file)
            request_data['u_img_url'] = profile_images.url(filename).replace("localhost:5000", server_name)

        # save the new user to DB or if an error during insertion to DB return error response
        new_user = User(request_data['u_id'], username, request_data['u_pwd'], request_data['u_img_url'])
        if new_user.save():
            data = User.get_schema().dump(new_user).data
            return build_response(False, "User created", data), status.HTTP_201_CREATED
        else:
            return build_response(False,
                                  "Failed to save user details, integrity error in DB"), status.HTTP_406_NOT_ACCEPTABLE

    @app.route('/users/login', methods=['POST'])
    def perform_login():
        request_data = request.get_json()
        username = request_data['username']
        # get the user object by username
        user = User.get_user_by_username(username)
        # check if username exists, if not return error response
        if user is None:
            return build_response(True, "No user with username: {}".format(username)), status.HTTP_204_NO_CONTENT
        # finally check that the password is correct password
        password = request_data['password']
        if user.check_password(password):
            data = User.get_schema().dump(user).data
            return build_response(False, "Login success", data), status.HTTP_200_OK
        else:
            return build_response(True, "Login failed"), status.HTTP_401_UNAUTHORIZED

    # gets, updates or deletes a user by id
    @app.route('/users/<uid>', methods=["GET", "PUT", "DELETE"])
    def single_user_operation(uid):
        # get user from DB to check if we can continue
        user = User.query.get(uid)
        if user is None:
            return build_response(True, "No user with id: {}".format(uid)), status.HTTP_204_NO_CONTENT
        if request.method == 'GET':
            data = User.get_schema().dump(user).data
            return build_response(False, "Retrieved user", data), status.HTTP_200_OK
        elif request.method == 'PUT':  # UPDATING A USER IN DB
            # updating a user requires sending the image and user data in a form, so get the request data
            request_data = json.loads(request.form.get("json"))
            # validate user fields, if invalid return error
            validation_errors = User.validate_user(request_data)
            if validation_errors:
                return validation_errors
            # get the user from DB, if not exist return error
            user = User.query.get(uid)
            if user is None:
                return build_response(True, "No user with id: {}".format(uid)), status.HTTP_204_NO_CONTENT
            # if usernames have changed update the username
            username = request_data['u_uname']
            if username != user.u_uname:
                username_exists = User.query.filter_by(u_uname=username).first()
                # if username already exists return an error 409 - Conflict
                if username_exists:
                    return build_response(True, "Username {} already exists".format(username)), status.HTTP_409_CONFLICT
                user.u_uname = username
            # save users profile image
            if 'u_img_url' in request.files:
                file = request.files['u_img_url']
                # file.filename = "{0}_{1}.jpg".format(uid, datetime.datetime.now().strftime("%d_%m_%Y_%H%M%S"))
                file.filename = "{}.jpg".format(request_data['u_id'])
                filename = profile_images.save(file)
                # TODO: Remove replace(...) when moving to server
                request_data['u_img_url'] = profile_images.url(filename).replace("localhost:5000", server_name)
                user.u_img_url = request_data['u_img_url']
            # attempt to update user
            if user.save():
                data = User.get_schema().dump(user).data
                return build_response(False, "User updated", data), status.HTTP_200_OK
            else:
                return build_response(False,
                                      "Failed to update user details, integrity error in DB"), status.HTTP_406_NOT_ACCEPTABLE
        elif request.method == 'DELETE':
            user.delete()
            return build_response(False, "User deleted"), status.HTTP_200_OK

    # gets all users threads
    @app.route('/users/<uid>/threads', methods=["GET"])
    def get_threads_by_author_id(uid):
        threads = Thread.query.filter_by(author_id=uid).all()
        if not threads:
            return build_response(True,
                                  "No threads found for author with id: {}".format(uid)), status.HTTP_204_NO_CONTENT
        data = Thread.get_schema(many=True).dump(threads).data
        return build_response(False, "Retrieved user threads", data), status.HTTP_200_OK

    # get, add or delete a users history data
    @app.route("/users/<uid>/history", methods=["GET", "POST", "DELETE"])
    def single_user_history_operation(uid):
        # get the user by id and make sure the user exists
        user = User.query.get(uid)
        if user is None:
            return build_response(True, "No user with id: {}".format(uid)), status.HTTP_204_NO_CONTENT
        if request.method == "GET":
            # get all the users history and return it in response
            history_items = History.get_all_by_user(uid)
            if not history_items:
                return build_response(False,
                                      "No history data in for user with id: {}".format(uid)), status.HTTP_204_NO_CONTENT
            data = History.get_schema(many=True).dump(history_items).data
            return build_response(False, "Retrieved history data", data), status.HTTP_200_OK
        elif request.method == "POST":
            # add the users history item, which includes uploading the plant image
            location = request.form.get("location")
            if 'img_upload' in request.files:
                file = request.files['img_upload']
                file.filename = "{0}_{1}.jpg".format(uid, datetime.datetime.now().strftime("%d_%m_%Y_%H%M%S"))
                # file.filename = "{}.jpg".format(uid)
                filename = id_images.save(file)
                history = History(id_images.url(filename).replace("localhost:5000", server_name), uid,
                                  location=location)
                history.save()
                return build_response(False, "Uploaded user history item.",
                                      History.get_schema().dump(history).data), status.HTTP_201_CREATED
            else:
                return build_response(True,
                                      "Error uploading image: file not found in request"), status.HTTP_400_BAD_REQUEST
        elif request.method == "DELETE":
            # delete user hostory item(s)
            request_data = request.get_json()
            history_list = []
            for history in request_data:
                history_list.append(History.query.get(history['h_id']))

            if not history_list:
                return build_response(True, "Could not parse history data"), status.HTTP_400_BAD_REQUEST

            for history in history_list:
                history.delete()

            return build_response(False, "All history items deleted from server DB"), status.HTTP_200_OK

    # Update a user history item
    @app.route("/users/<uid>/history/<hid>", methods=["PUT"])
    def update_user_history_item(uid, hid):
        # get user from DB and make sure the user exists
        user = User.query.get(uid)
        if user is None:
            return build_response(True, "No user with id: {}".format(uid)), status.HTTP_204_NO_CONTENT
        history = History.query.get(hid)
        if history is None:
            return build_response(True, "No history item with id: {}".format(hid)), status.HTTP_204_NO_CONTENT
        request_data = request.get_json()
        validation_errors = History.validate_history(request_data)
        if validation_errors:
            return validation_errors
        history.h_is_matched = request_data['h_is_matched']
        history.h_plant_id = request_data['h_plant_id']
        history.save()
        data = History.get_schema().dump(history).data
        return build_response(False, "History item updated successfully.", data), status.HTTP_200_OK

    ########################################### Plants ####################################################

    # endpoint to fill plant data into DB, requires authentication
    @app.route("/plants", methods=["POST"])
    @auth.login_required
    def add_plants():
        request_data = request.get_json()
        plants = request_data['data']
        plants_list = []
        # the plant object has too many fields to pass manually so pass the dictionary and set the keys as fields
        # names and values as the field values
        for plant in plants:
            plants_list.append(Plant(**plant))
        if not plants_list:
            return build_response(True, "Could not parse plants data"), status.HTTP_400_BAD_REQUEST

        for plant in plants_list:
            if not plant.save():
                return build_response(True, "An item already exists - exited"), status.HTTP_409_CONFLICT

        return build_response(False, "All plants added to server DB"), status.HTTP_201_CREATED

    # endpoint to show all plants
    @app.route("/plants", methods=["GET"])
    def get_plants():
        all_plants = Plant.get_all()
        if not all_plants:
            return build_response(False, "No plant data in DB"), status.HTTP_204_NO_CONTENT

        data = Plant.get_schema(many=True).dump(all_plants).data
        return build_response(False, "Retrieved plants data", data), status.HTTP_200_OK

    # gets a plant by id
    @app.route('/plants/<pid>', methods=["GET"])
    def get_plant_by_id(pid):
        plant = Plant.query.get(pid)
        if plant is None:
            return build_response(True, "No plant with id: {}".format(pid)), status.HTTP_204_NO_CONTENT
        data = Plant.get_schema().dump(plant).data
        return build_response(False, "Retrieved plant", data), status.HTTP_200_OK

    ########################################### Threads ####################################################

    # endpoint to create or get a thread
    @app.route("/threads", methods=["GET", "POST"])
    def all_threads_operation():
        if request.method == 'POST':
            # create a new thread, first validate the data in the request
            request_data = request.get_json()
            validation_errors = Thread.validate_thread(request_data)
            if validation_errors:
                return validation_errors
            new_thread = Thread(request_data['id'], request_data['t_title'], request_data['message'],
                                request_data['author_id'], request_data['t_img_url'], request_data['t_solved'],
                                request_data['t_location'])
            # attempt to save thread, it could be an update so check here (see below comments)
            if not new_thread.save():
                from shared import db  # import to be able to rollback
                """ 
                    This is required as syncing changes from client assumes the thread is new, this allows for thread
                    to be updated using POST. Only updates from client sync otherwise PUT is used.
                    rollback from previous save fail, thread must already exist
                """
                db.session.rollback()
                # already exists so get the thread by ID and update values
                thread = Thread.query.get(request_data['id'])
                thread.t_title = request_data['t_title']
                thread.message = request_data['message']
                thread.t_solved = request_data['t_solved']
                if thread.save():
                    data = Thread.get_schema().dump(thread).data
                    return build_response(False, "Thread created", data), status.HTTP_200_OK
                else:
                    return build_response(True, "Item already exists"), status.HTTP_409_CONFLICT
            data = Thread.get_schema().dump(new_thread).data
            # unknown issue user mustn't have been created
            if not data:
                return build_response(False, "Warning - User may not have been created"), status.HTTP_204_NO_CONTENT
            return build_response(False, "Thread created", data), status.HTTP_201_CREATED
        elif request.method == "GET":
            # gets all threads
            all_threads = Thread.get_all()
            if not all_threads:
                return build_response(True, "No threads found."), status.HTTP_204_NO_CONTENT

            data = Thread.get_schema(many=True).dump(all_threads).data
            return build_response(False, "Retrieved threads", data), status.HTTP_200_OK

    # gets, updates or deletes a thread by id
    @app.route('/threads/<tid>', methods=["GET", "PUT", "DELETE"])
    def single_thread_operation(tid):
        if request.method == 'GET':
            # get the thread an return it
            thread = Thread.query.get(tid)
            if not thread:
                return build_response(True, "No thread with id: {}".format(tid)), status.HTTP_204_NO_CONTENT
            data = Thread.get_schema().dump(thread).data
            return build_response(False, "Retrieved thread", data), status.HTTP_200_OK
        elif request.method == 'PUT':
            # updating thread, ensure all fields are valid
            request_data = request.get_json()
            validation_errors = Thread.validate_thread(request_data)
            if validation_errors:
                return validation_errors
            thread = Thread.query.get(tid)
            if not thread:
                return build_response(True, "No thread found with id: {}".format(tid)), status.HTTP_204_NO_CONTENT
            thread.t_title = request_data['t_title']
            thread.t_img_url = request_data['t_img_url']
            thread.message = request_data['message']
            thread.t_solved = request_data['t_solved']
            thread.save()
            data = Thread.get_schema().dump(thread).data
            return build_response(False, "OK", data), status.HTTP_200_OK
        elif request.method == 'DELETE':
            thread = Thread.query.get(tid)
            if thread is None:
                return build_response(True, "No thread with id: {}".format(tid)), status.HTTP_204_NO_CONTENT
            thread.delete()
            return build_response(False, "Thread deleted"), status.HTTP_200_OK

    ########################################### Comments ####################################################

    # gets all threads comments or adds a comment
    @app.route('/threads/<tid>/comments', methods=["GET", "POST"])
    def comment_operation_for_thread_id(tid):
        if request.method == 'GET':
            # get all the comments by thread id and return them
            comments = Comment.get_all_by_thread_id(tid)
            if not comments:
                return build_response(True, "Not comments found for thread with id {}".format(
                    tid)), status.HTTP_204_NO_CONTENT
            data = Comment.get_schema(many=True).dump(comments).data
            return build_response(False, "OK", data), status.HTTP_200_OK
        elif request.method == 'POST':
            # check that the thread exists, if not return erro response
            thread_exists = Thread.query.get(tid)
            if not thread_exists:
                return build_response(True, "No thread exists with id: {}".format(tid)), status.HTTP_204_NO_CONTENT
            # validate comments and ensure the thread id matches the comment thread id
            request_data = request.get_json()
            validation_errors = Comment.validate_comment(request_data)
            if validation_errors:
                return validation_errors
            # comments thread id doesn't match the thread it is being added to
            if str(request_data['c_thread_id']) != str(tid):
                return build_response(True,
                                      "Attempting to add comment to thread with different ID"), status.HTTP_400_BAD_REQUEST
            new_comment = Comment(request_data['id'], request_data['message'], request_data['author_id'],
                                  request_data['c_is_answer'], tid)
            """
            Likewise with above syncing from client assumes that comments are new, if an error occurs during
            saving the comment it is due to the id already existing. Therefore rollback and update the comment.
            PUT is still used by client for updating a comment normally.
            """
            if not new_comment.save():
                from shared import db
                # rollback session after failed save
                db.session.rollback()
                # comment already exists so get the comment and modify then update
                # this is to fix syncing from client
                comment = Comment.query.get(request_data['id'])
                comment.message = request_data['message']
                comment.c_is_answer = request_data['c_is_answer']
                if comment.save():
                    return build_response(False, "Comment created",
                                          Comment.get_schema().dumps(comment).data), status.HTTP_200_OK
                else:
                    return build_response(True, "Error attempting to update comment"), status.HTTP_409_CONFLICT

            data = Comment.get_schema().dump(new_comment).data
            return build_response(False, "Comment created", data), status.HTTP_201_CREATED

    # get, update or delete a comment with a given id
    @app.route('/threads/<tid>/comments/<cid>', methods=["GET", "PUT", "DELETE"])
    def delete_comment_by_id(tid, cid):
        if request.method == 'GET':
            comment = Comment.query.get(cid)
            if not comment:
                return build_response(True, "No comment with id: {}".format(cid)), status.HTTP_204_NO_CONTENT
            data = Comment.get_schema().dump(comment).data
            return build_response(False, "Retrieved comment", data), status.HTTP_200_OK
        elif request.method == 'DELETE':
            comment = Comment.query.get(cid)
            if comment is None:
                return build_response(True, "No comment with id: {}".format(cid)), status.HTTP_204_NO_CONTENT
            comment.delete()
            return build_response(False, "Comment deleted"), status.HTTP_200_OK
        elif request.method == 'PUT':
            thread_exists = Thread.query.get(tid)
            if not thread_exists:
                return build_response(True, "No thread exists with id: {}".format(tid)), status.HTTP_204_NO_CONTENT
            comment = Comment.query.get(cid)
            if not comment:
                return build_response(True, "No comment exists with id: {}".format(cid)), status.HTTP_204_NO_CONTENT
            request_data = request.get_json()
            validation_errors = Comment.validate_comment(request_data)
            if validation_errors:
                return validation_errors
            # comments thread id doesn't match the thread it is being added to
            if str(request_data['c_thread_id']) != str(tid):
                return build_response(True,
                                      "Attempting to add comment to thread with different ID"), status.HTTP_400_BAD_REQUEST
            comment.c_is_answer = request_data['c_is_answer']
            comment.message = request_data['message']
            # error saving comment
            if not comment.save():
                return build_response(True, "Error updating item"), status.HTTP_409_CONFLICT
            data = Comment.get_schema().dump(comment).data
            return build_response(False, "Comment updated", data), status.HTTP_200_OK


# Helper functions

# builds a response sent back to the client
# -- error - request was successful or not??
# -- message - OK or actual error??
# -- data - enveloped data to return??
def build_response(error, message, data=''):
    response_data = dict()
    response_data['error'] = error
    response_data['message'] = message
    response_data['data'] = data
    print(message)
    print(data)
    return jsonify(response_data)


# reads the credentials from a file, the credentials are used for adding plants to the remote
def read_credentials():
    from shared import CREDENTIALS_FILE
    credentials = {}
    with open(CREDENTIALS_FILE) as file:
        for line in file:
            # ignore line comments or blank lines
            if len(line) == 0 or line.startswith('#'):
                continue
            (key, val) = line.split()
            credentials[key] = val
    return credentials
